{% set location = 'eastus' %}
{% set rg_name = 't0-idem-test' %}
{% set vnet_name = 't0-idem-test-network' %}
{% set public_ip_name = 't0-idem-test-public-ip' %}
{% set ip_config_name = "t0-idem-test-ip-config" %}
{% set nic_name = 't0-idem-test-nic' %}
{% set subnet_name = 't0-idem-test-subnet' %}
{% set vm_name = 't0-idem-test-vm' %}
{% set vm_user_name = 'ubuntu' %}
{% set vm_user_passwd = 'change@me1' %}
{% set sls_rev = '1.0.1' %}
{% set vm_size = "Standard_B1ls" %}

Assure Resource Group Present:
  azure.resource.resource_groups.present:
  - resource_group_name: {{ rg_name }}
  - parameters:
      location: {{ location }}
      tags:
          env: idem-test
          sls-rev: {{ sls_rev }}

Assure Virtual Network Present:
  azure.network.virtual_networks.present:
  - resource_group_name: {{ rg_name }}
  - virtual_network_name: {{ vnet_name }}
  - parameters:
      location: {{ location }}
      address_space:
        address_prefixes: ["10.0.0.0/16"]

Assure Subnet Present:
  azure.network.subnets.present:
  - resource_group_name: {{ rg_name }}
  - virtual_network_name: {{ vnet_name }}
  - subnet_name: {{ subnet_name }}
  - subnet_parameters:
      address_prefix: "10.0.0.0/24"

Assure Public IP Address Present:
  azure.network.public_ip_addresses.present:
  - resource_group_name: {{ rg_name }}
  - public_ip_address_name: {{ public_ip_name }}
  - parameters:
      location: {{ location }}
      sku:
        name: Standard
      public_ip_allocation_method: Static
      public_ip_address_version : IPv4

Assure Network Interface Present:
  azure.network.network_interfaces.present:
  - resource_group_name: {{ rg_name }}
  - network_interface_name: {{ nic_name }}
  - parameters:
      location: {{ location }}
      ip_configurations:
        - name: {{ ip_config_name }}
          subnet:
            id:
            - {{rg_name}}
            - {{vnet_name}}
            - {{subnet_name}}
          public_ip_address:
            id:
            - {{rg_name}}
            - {{public_ip_name}}
      # tags:
      #   Organization: "Everest"
      #   Owner: "Elmer Fudd Gantry"

Assure Azure VM Present:
  azure.compute.virtual_machines.present:
  - resource_group_name: {{ rg_name }}
  - vm_name: {{ vm_name }}
  - parameters:
      location: {{ location }}
      storage_profile:
        image_reference:
          publisher: debian
          offer: debian-10
          sku: "10-backports"
          version: latest
      hardware_profile:
        vm_size: {{ vm_size }}
      os_profile:
        computer_name: {{ vm_name }}
        admin_username: {{ vm_user_name }}
        admin_password: {{ vm_user_passwd }}
      network_profile:
        network_interfaces:
        - primary: true
          delete_option: "Detach"
          id:
          - {{ rg_name }}
          - {{ nic_name }}
      # tags:
      #   Organization: "Everest"
      #   Owner: "Elmer Fudd Gantry"
