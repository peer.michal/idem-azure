"""
Azure Resource Manager (AzureRM) pytest configurations.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import logging
import os
import sys

CODE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
TPATH_DIR = os.path.join(os.path.dirname(__file__), "tpath")

if CODE_DIR in sys.path:
    sys.path.remove(CODE_DIR)
sys.path.insert(0, CODE_DIR)
sys.path.insert(0, TPATH_DIR)

import pytest


log = logging.getLogger("pop.tests")


def pytest_runtest_protocol(item, nextitem):
    """
    implements the runtest_setup/call/teardown protocol for
    the given test item, including capturing exceptions and calling
    reporting hooks.
    """
    log.debug(f">>>>> START >>>>> {item.name}")


def pytest_runtest_teardown(item):
    """
    called after ``pytest_runtest_call``
    """
    log.debug(f"<<<<< END <<<<<<< {item.name}")


def pytest_configure(config):
    config.addinivalue_line("markers", "first: mark test to run first")
    config.addinivalue_line("markers", "second: mark test to run second")
    config.addinivalue_line(
        "markers", "second_to_last: mark test to run second to last"
    )
    config.addinivalue_line("markers", "last: mark test to run last")
    config.addinivalue_line(
        "markers", "slow: mark tests with an above average run time"
    )


@pytest.fixture
def os_sleep_secs():
    if "CI_RUN" in os.environ:
        return 1.75
    return 0.5


@pytest.fixture(scope="session")
def acct_subs():
    yield ["azure"]


@pytest.fixture(scope="session")
def acct_profile():
    yield "default"


@pytest.fixture(scope="session")
def tags():
    yield {
        "Organization": "Everest",
        "Owner": "Elmer Fudd Gantry",
    }


@pytest.fixture(scope="session")
def hub(hub):
    """
    Overriding pytest-pop's hub to add in this plugin's subs
    :param hub: hub from pytest-pop
    :return: yields the test hub plus our subs
    """

    for dyne in ("idem", "exec", "states", "tool", "acct"):
        hub.pop.sub.add(dyne_name=dyne)
        hub.pop.sub.load_subdirs(getattr(hub, dyne), recurse=True)

    # hub.pop.sub.load_subdirs(getattr(hub, "exec"), recurse=True)
    # hub.pop.sub.load_subdirs(getattr(hub, "tool"), recurse=True)

    hub.log.debug(f"{__file__}: {os.curdir}")

    yield hub
