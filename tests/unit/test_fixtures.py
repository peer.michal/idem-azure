"""
Azure Resource Manager (AzureRM) pytest fixtures tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.asyncio
async def test_hub_fixture_load(hub):
    assert hub.tool.azure
    assert hub.states.azure
    assert hub.exec.azure
    # assert hub.acct.azure_keyvault


@pytest.mark.asyncio
async def test_mock_hub_fixture_load(mock_hub):
    assert mock_hub.tool.azure
    assert mock_hub.states.azure
    assert mock_hub.exec.azure
    # assert mock_hub.acct.azure_keyvault
