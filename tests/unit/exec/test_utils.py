"""
Azure Resource Manager (AzureRM) exec module tests.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


async def _xform_object(hub, ctx, value):
    if isinstance(value, str):
        ret = value
    else:
        ret = "".join(value)

    return ret


@pytest.mark.asyncio
async def test_transform_object(hub, ctx):
    testval = {
        "DontTransform": "Right.",
        "Go down a level and ": [
            {"I": ("got ", "xformed.")},
            {"But": "I did not."},
            {"this", "is", "a", "parent", "sized", "set"},
        ],
    }
    expected = {
        "DontTransform": "Right.",
        "Go down a level and ": [
            {"I": "got xformed."},
            {"But": "I did not."},
            {"this", "is", "a", "parent", "sized", "set"},
        ],
    }
    ret = await hub.exec.azure.utils.transform_object(ctx, testval, _xform_object, "I")
    assert ret["result"]
    assert expected == ret["ret"]


def test_is_within(hub):
    parent = {
        "k1": "this is key1",
        "list1": ["this", "is", "a", "list", "with another", "1"],
        "dict1": {"k2_1": "key 2_1", "k2_2": "key 2_2"},
        "set1": {"this", "is", "a", "parent", "sized", "set"},
    }
    child1 = {"list1": "not really a list"}
    child2 = {
        "list1": ["this", "is", "list", "1"],
    }
    child3 = {"dict1": {"k2_1": "key 2_1", "k2_2": "key 2_2"}}
    child4 = {"k1": "this is key1"}
    child5 = {"set1": {"this", "is", "a", "set"}}
    child6 = {"set2": {"this", "is", "another", "set"}}
    assert not hub.tool.azure.utils.is_within(parent, child1)
    assert hub.tool.azure.utils.is_within(parent, child2)
    assert hub.tool.azure.utils.is_within(parent, child3)
    assert hub.tool.azure.utils.is_within(parent, child4)
    assert hub.tool.azure.utils.is_within(parent, child4)
    assert hub.tool.azure.utils.is_within(parent, child5)
    assert hub.tool.azure.utils.is_within(parent, child6, {"set2"})
