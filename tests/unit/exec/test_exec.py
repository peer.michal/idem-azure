"""
Azure Resource Manager (AzureRM) exec module tests.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.asyncio
async def test_exec(hub, ctx):
    # Build multiple calls to assure they get created.
    call1 = hub.exec.azure.compute.virtual_machines.list
    assert call1
    call2 = hub.exec.azure.compute.virtual_machines.begin_create_or_update
    assert call2
    call3 = hub.exec.azure.compute.virtual_machines.list
    assert call1 == call3
