"""
Azure Resource Manager (AzureRM) resource_groups tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import asyncio

import pytest
from pop import exc


def async_return(result):
    f = asyncio.Future()
    f.set_result(result)
    return f


@pytest.fixture(scope="function")
def name():
    yield "group_a"


@pytest.fixture(scope="function")
def location():
    yield "eastus"


@pytest.mark.asyncio
async def test_present(mocker, hub, ctx, name, location, tags):
    parameters = {"location": location, "tags": tags}

    rg = {"id": name, "name": name, "location": location, "tags": tags}

    _check_existence = {"result": True, "ret": True, "comment": ""}
    _get = {"result": True, "ret": rg, "comment": ""}

    expected = {
        "name": name,
        "result": True,
        "comment": f"Resource group {name} is already present.",
        "changes": {},
    }

    # Fails because of required args
    with pytest.raises(exc.BindError, match=r".*missing .* required argument.*"):
        ret = await hub.states.azure.resource.resource_groups.present(name, parameters)

    # Force the execs to dynamically create the relevant Sub and mock it.
    hub.exec.azure.resource.resource_groups.check_existence
    mocker.patch.object(hub.exec.azure.resource.resource_groups, "check_existence")
    hub.exec.azure.resource.resource_groups.check_existence.return_value = async_return(
        _check_existence
    )

    hub.exec.azure.resource.resource_groups.get
    mocker.patch.object(hub.exec.azure.resource.resource_groups, "get")
    hub.exec.azure.resource.resource_groups.get.return_value = async_return(_get)

    hub.exec.azure.resource.resource_groups.create_or_update
    mocker.patch.object(hub.exec.azure.resource.resource_groups, "create_or_update")
    hub.exec.azure.resource.resource_groups.get.return_value = async_return(_get)

    ret = await hub.states.azure.resource.resource_groups.present(
        ctx, name, name, parameters
    )

    assert ret == expected


async def test_absent(mock_hub, ctx):
    ...
