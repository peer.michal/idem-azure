"""
Azure Resource Manager (AzureRM) pytest configurations.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import asyncio
import random
import string

import pytest


@pytest.fixture(scope="session")
def event_loop():
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def location():
    yield "eastus"


@pytest.fixture(scope="session")
async def resource_group(hub, ctx, location):
    name = "rg-idem-inttest-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )

    parameters = {"location": location}

    await hub.states.azure.resource.resource_groups.present(ctx, name, name, parameters)
    yield name

    # Clean up
    await hub.states.azure.resource.resource_groups.absent(ctx, name, name)


@pytest.fixture(scope="session")
async def vm(hub, ctx, resource_group, network_interface, location, password):
    name = "vm-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(5)
    )
    params = {
        "location": location,
        "storage_profile": {
            "image_reference": {
                "publisher": "debian",
                "offer": "debian-10",
                "sku": "10-backports",
                "version": "latest",
            }
        },
        "hardware_profile": {"vm_size": "Standard_B1ls"},
        "os_profile": {
            "computer_name": name,
            "admin_username": "vmware",
            "admin_password": password,
        },
        "network_profile": {
            "network_interfaces": [
                {
                    "id": [resource_group, network_interface],
                    "primary": True,
                    "delete_option": "Detach",
                }
            ]
        },
    }
    ret = await hub.states.azure.compute.virtual_machines.present(
        ctx, name, resource_group, name, params
    )

    yield name

    # Clean up
    await hub.states.azure.compute.virtual_machines.absent(
        ctx, name, resource_group, name
    )


@pytest.fixture(scope="session")
def keyvault():
    yield "kv-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def storage_account():
    yield "stidem" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(16)
    )


@pytest.fixture(scope="session")
def storage_container():
    yield "container-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(32)
    )


@pytest.fixture(scope="session")
def log_analytics_workspace():
    yield "log-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(32)
    )


@pytest.fixture(scope="session")
def postgresql_server():
    yield "psql-server-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
async def vnet(hub, ctx, resource_group, location):
    name = "vnet-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )
    parameters = {
        "location": location,
        "address_space": {"address_prefixes": ["10.0.0.0/16"]},
    }
    await hub.states.azure.network.virtual_networks.present(
        ctx, name, resource_group, name, parameters
    )
    yield name

    # Clean up
    await hub.states.azure.network.virtual_networks.absent(
        ctx, name, resource_group, name
    )


@pytest.fixture(scope="session")
async def vnet2(hub, ctx, resource_group, location):
    name = "vnet-idem2-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )
    parameters = {
        "location": location,
        "address_space": {"address_prefixes": ["10.1.0.0/16"]},
    }
    await hub.states.azure.network.virtual_networks.present(
        ctx, name, resource_group, name, parameters
    )
    yield name

    # Clean up
    await hub.states.azure.network.virtual_networks.absent(
        ctx, name, resource_group, name
    )


@pytest.fixture(scope="session")
async def subnet(hub, ctx, resource_group, vnet, location):
    name = "snet-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )
    parameters = {"address_prefix": "10.0.0.0/24"}
    await hub.states.azure.network.subnets.present(
        ctx, name, resource_group, vnet, name, parameters
    )
    yield name

    # Clean up
    await hub.states.azure.network.subnets.absent(ctx, name, resource_group, vnet, name)


@pytest.fixture(scope="session")
async def public_ip_addr(hub, ctx, resource_group, location):
    name = "pip-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )
    parameters = {
        "location": location,
        "sku": {"name": "Standard", "tier": "Regional"},
        "public_ip_allocation_method": "Static",
        "public_ip_address_version": "IPv4",
    }
    await hub.states.azure.network.public_ip_addresses.present(
        ctx, name, resource_group, name, parameters
    )
    yield name

    # Clean up
    await hub.states.azure.network.public_ip_addresses.absent(
        ctx, name, resource_group, name
    )


@pytest.fixture(scope="session")
async def public_ip_addr2(hub, ctx, resource_group, location):
    name = "pip-idem-2-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )
    parameters = {
        "location": location,
        "sku": {"name": "Standard"},
        "public_ip_allocation_method": "Static",
        "public_ip_address_version": "IPv4",
    }
    ret = await hub.states.azure.network.public_ip_addresses.present(
        ctx, name, resource_group, name, parameters
    )
    yield name

    # Clean up
    await hub.states.azure.network.public_ip_addresses.absent(
        ctx, name, resource_group, name
    )


@pytest.fixture(scope="session")
def route_table():
    yield "rt-table-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def route():
    yield "rt-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def load_balancer():
    yield "lb-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def zone():
    yield "zone.idem." + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def availability_set():
    yield "avail-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
async def network_interface(
    hub, ctx, resource_group, vnet, subnet, public_ip_addr, ip_config, location
):
    name = "nic-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )
    params = {
        "location": location,
        "ip_configurations": [
            {
                "name": ip_config,
                "subnet": {"id": [resource_group, vnet, subnet]},
                "public_ip_address": {"id": [resource_group, public_ip_addr]},
            }
        ],
    }
    await hub.states.azure.network.network_interfaces.present(
        ctx, name, resource_group, name, params
    )
    yield name

    # Clean up
    await hub.states.azure.network.network_interfaces.absent(
        ctx, name, resource_group, name
    )


@pytest.fixture(scope="session")
def local_network_gateway():
    yield "lgw-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def ip_config():
    yield "ip-config-idem-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def acr():
    yield "acrideminttest" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(8)
    )


@pytest.fixture(scope="session")
def password():
    yield "#PASS" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(16)
    ) + "!"
