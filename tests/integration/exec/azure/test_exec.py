"""
Azure Resource Manager (AzureRM) exec tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import uuid

import pytest


@pytest.mark.asyncio
async def test_resource_group_check_existence(hub, ctx):
    """
    Tests that the exec dynamic sub creation works. At least two calls are needed
    to assure multiple recursive call pathways succeed.
    """
    rg = str(uuid.uuid4())

    exists_expected = {"result": True, "ret": False, "comment": ""}
    ret = await hub.exec.azure.resource.resource_groups.check_existence(ctx, rg)
    assert ret == exists_expected

    get_expected = {"result": True, "ret": None, "comment": ""}
    ret = await hub.exec.azure.resource.resource_groups.get(ctx, rg)
    assert ret == get_expected
