"""
Azure Resource Manager (AzureRM) state tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.order(order=1)
@pytest.mark.asyncio
async def test_present(
    hub,
    ctx,
    resource_group,
    vnet,
    subnet,
    public_ip_addr,
    ip_config,
    network_interface,
    location,
):
    params = {
        "location": location,
        "ip_configurations": [
            {
                "name": ip_config,
                "subnet": {"id": [resource_group, vnet, subnet]},
                "public_ip_address": {"id": [resource_group, public_ip_addr]},
            }
        ],
    }
    ret = await hub.states.azure.network.network_interfaces.present(
        ctx, network_interface, resource_group, network_interface, params
    )
    assert ret["result"]
    assert ret["name"] == network_interface
    assert (
        ret["comment"] == f"Network Interface {network_interface} is already present."
    )


@pytest.mark.order(order=1, after="test_present", before="test_absent")
@pytest.mark.asyncio
async def test_changes(
    hub,
    ctx,
    resource_group,
    vnet,
    subnet,
    public_ip_addr,
    ip_config,
    network_interface,
    tags,
    location,
):
    params = {
        "location": location,
        "ip_configurations": [
            {
                "name": ip_config,
                "subnet": {"id": [resource_group, vnet, subnet]},
                "public_ip_address": {"id": [resource_group, public_ip_addr]},
            }
        ],
        "tags": tags,
    }
    ret = await hub.states.azure.network.network_interfaces.present(
        ctx, network_interface, resource_group, network_interface, params
    )
    assert ret["result"]
    assert ret["name"] == network_interface
    assert ret["comment"] == f"Network Interface {network_interface} has been updated."
    assert ret["changes"]["new"]["tags"] == tags


@pytest.mark.order(order=-1)
@pytest.mark.asyncio
async def test_absent(hub, ctx, resource_group, network_interface):
    ret = await hub.states.azure.network.network_interfaces.absent(
        ctx, network_interface, resource_group, network_interface
    )
    assert ret["result"]
    assert ret["name"] == network_interface
    assert (
        ret["comment"] == f"Network Interface {network_interface} delete in progress."
    )
