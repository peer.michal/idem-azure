"""
Azure Resource Manager (AzureRM) state tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.order(order=2)
@pytest.mark.asyncio
async def test_present(hub, ctx, resource_group, vnet, subnet, location):
    params = {"address_prefix": "10.0.0.0/24"}
    ret = await hub.states.azure.network.subnets.present(
        ctx, subnet, resource_group, vnet, subnet, params
    )
    assert ret["result"]
    assert ret["name"] == subnet
    assert ret["comment"] == f"Subnet {subnet} is already present."


@pytest.mark.order(order=2, after="test_present", before="test_absent")
@pytest.mark.asyncio
async def test_changes(hub, ctx, resource_group, vnet, subnet):
    parameters = {"address_prefix": "10.0.1.0/24"}
    ret = await hub.states.azure.network.subnets.present(
        ctx, subnet, resource_group, vnet, subnet, parameters
    )
    assert ret["result"]
    assert ret["name"] == subnet
    assert ret["changes"]["new"]["address_prefix"] == parameters["address_prefix"]


@pytest.mark.order(order=-2)
@pytest.mark.asyncio
async def test_absent(hub, ctx, resource_group, vnet, subnet, location):
    ret = await hub.states.azure.network.subnets.absent(
        ctx, subnet, resource_group, vnet, subnet
    )
    assert ret["result"]
    assert ret["name"] == subnet
    assert ret["comment"] == f"Subnet {subnet} delete in progress."
