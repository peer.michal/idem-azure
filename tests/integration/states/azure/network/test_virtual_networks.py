"""
Azure Resource Manager (AzureRM) state tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.order(order=1)
@pytest.mark.asyncio
async def test_present(hub, ctx, resource_group, vnet, location):
    params = {
        "location": location,
        "address_space": {"address_prefixes": ["10.0.0.0/16"]},
    }
    ret = await hub.states.azure.network.virtual_networks.present(
        ctx, vnet, resource_group, vnet, params
    )
    assert ret["result"]
    assert ret["name"] == vnet
    assert ret["comment"] == f"Virtual network {vnet} is already present."


@pytest.mark.order(order=1, after="test_present", before="test_absent")
@pytest.mark.asyncio
async def test_changes(hub, ctx, resource_group, vnet, location, tags):
    parameters = {
        "address_space": {"address_prefixes": ["10.0.0.0/16"]},
        "location": location,
        "tags": tags,
    }
    ret = await hub.states.azure.network.virtual_networks.present(
        ctx, vnet, resource_group, vnet, parameters
    )
    assert ret["result"]
    assert ret["name"] == vnet
    assert ret["changes"]["new"]["tags"] == tags


@pytest.mark.order(order=-1)
@pytest.mark.asyncio
async def test_absent(hub, ctx, resource_group, vnet, location):
    ret = await hub.states.azure.network.virtual_networks.absent(
        ctx, vnet, resource_group, vnet
    )
    assert ret["result"]
    assert ret["name"] == vnet
    assert ret["comment"] == f"Virtual network {vnet} delete in progress."
