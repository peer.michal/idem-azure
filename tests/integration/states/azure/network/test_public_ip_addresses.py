"""
Azure Resource Manager (AzureRM) state tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.order(order=1)
@pytest.mark.asyncio
async def test_present(hub, ctx, resource_group, public_ip_addr, location):
    params = {
        "location": location,
        "sku": {"name": "Standard", "tier": "Regional"},
        "public_ip_allocation_method": "Static",
        "public_ip_address_version": "IPv4",
    }
    ret = await hub.states.azure.network.public_ip_addresses.present(
        ctx, public_ip_addr, resource_group, public_ip_addr, params
    )
    assert ret["result"]
    assert ret["name"] == public_ip_addr
    assert ret["comment"] == f"Public IP Address {public_ip_addr} is already present."


@pytest.mark.order(order=1, after="test_present", before="test_absent")
@pytest.mark.asyncio
async def test_changes(hub, ctx, resource_group, public_ip_addr, location, tags):
    params = {
        "location": location,
        "sku": {"name": "Standard", "tier": "Regional"},
        "public_ip_allocation_method": "Static",
        "public_ip_address_version": "IPv4",
        "tags": tags,
    }
    ret = await hub.states.azure.network.public_ip_addresses.present(
        ctx, public_ip_addr, resource_group, public_ip_addr, params
    )
    assert ret["result"]
    assert ret["name"] == public_ip_addr
    assert ret["changes"]["new"]["tags"] == tags


@pytest.mark.order(order=-1)
@pytest.mark.asyncio
async def test_absent(hub, ctx, resource_group, public_ip_addr):
    ret = await hub.states.azure.network.public_ip_addresses.absent(
        ctx, public_ip_addr, resource_group, public_ip_addr
    )
    assert ret["result"]
    assert ret["name"] == public_ip_addr
    assert ret["comment"] == f"Public IP Address {public_ip_addr} delete in progress."
