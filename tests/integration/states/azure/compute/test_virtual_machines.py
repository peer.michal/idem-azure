"""
Azure Resource Manager (AzureRM) state tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.order(order=3)
@pytest.mark.asyncio
async def test_present(
    hub, ctx, resource_group, vm, network_interface, location, password
):
    params = {
        "location": location,
        "storage_profile": {
            "image_reference": {
                "publisher": "debian",
                "offer": "debian-10",
                "sku": "10-backports",
                "version": "latest",
            }
        },
        "hardware_profile": {"vm_size": "Standard_B1ls"},
        "os_profile": {
            "computer_name": vm,
            "admin_username": "vmware",
            "admin_password": password,
        },
        "network_profile": {
            "network_interfaces": [
                {
                    "id": [resource_group, network_interface],
                    "primary": True,
                    "delete_option": "Detach",
                }
            ]
        },
    }
    ret = await hub.states.azure.compute.virtual_machines.present(
        ctx, vm, resource_group, vm, params
    )
    assert ret["result"]
    assert ret["name"] == vm
    assert ret["comment"] == f"Virtual Machine {vm} is already present."


@pytest.mark.order(order=3, after="test_present", before="test_absent")
@pytest.mark.asyncio
async def test_changes(hub, ctx, resource_group, vm, tags):
    params = {"tags": tags}
    ret = await hub.states.azure.compute.virtual_machines.present(
        ctx, vm, resource_group, vm, params
    )
    assert ret["result"]
    assert ret["name"] == vm
    assert ret["comment"] == f"Virtual Machine {vm} has been updated."
    assert ret["changes"]["new"]["tags"] == tags


@pytest.mark.order(order=-3)
@pytest.mark.asyncio
async def test_absent(hub, ctx, resource_group, vm):
    ret = await hub.states.azure.compute.virtual_machines.absent(
        ctx, vm, resource_group, vm
    )
    assert ret["result"]
    assert ret["name"] == vm
    assert (ret["comment"]) == f"Virtual Machine {vm} delete in progress."
