"""
Azure Resource Manager (AzureRM) state tests.

Copyright (c) 2019-2020, EITR Technologies, LLC.
Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
import pytest


@pytest.mark.order(order=1)
@pytest.mark.asyncio
async def test_present(hub, ctx, resource_group, location):
    expected = {
        "name": resource_group,
        "result": True,
        "comment": f"Resource group {resource_group} is already present.",
        "changes": {},  # No changes because fixture created the grouop.
    }

    parameters = {"location": location}
    ret = await hub.states.azure.resource.resource_groups.present(
        ctx, resource_group, resource_group, parameters
    )
    assert ret == expected


@pytest.mark.order(order=1, after="test_present", before="test_absent")
@pytest.mark.asyncio
async def test_changes(hub, ctx, resource_group, location, tags):
    parameters = {"location": location, "tags": tags}
    ret = await hub.states.azure.resource.resource_groups.present(
        ctx, resource_group, resource_group, parameters
    )
    assert ret["changes"]["new"]["tags"] == tags


@pytest.mark.order(order=-1)
@pytest.mark.asyncio
async def test_absent(hub, ctx, resource_group, location, tags):
    ret = await hub.states.azure.resource.resource_groups.absent(
        ctx, resource_group, resource_group
    )
    assert ret["result"]
    assert ret["name"] == resource_group
    assert ret["comment"] == f"Resource group {resource_group} delete in progress."
